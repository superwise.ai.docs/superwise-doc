## Simulator notebook for how to start working with superwise SDK
* Link to the notebook: [Notebook!](https://storage.googleapis.com/sw-example/sdk-example.ipynb)
### Install requirements packages


```python
! pip install superwise
! pip install cloudpickle
! pip install nest_asyncio
```

##### *Config environment variables*


```python
import os
os.environ["SW_HOST"] = "<sub dommain >.superwise.ai"
os.environ["SW_TOKEN"] = "<token>"
```

### Imports requirements packages


```python
from superwise.superwise import Superwise
import pandas as pd
import cloudpickle as cp
import json
from ipywidgets import IntProgress
import nest_asyncio
nest_asyncio.apply()
 

from urllib.request import urlopen
```

### Loading

- Data


```python
data = pd.read_csv('https://storage.googleapis.com/superwise/sdk/simulator_titanic_demo_task.csv')
print(data.shape)
print(data.columns)

FEATURES = ['Pclass', 'Age', 'SibSp', 'Parch', 'Sex_female', 'Sex_male', 'Embarked_C', 'Embarked_Q', 'Embarked_S']
```

    (669, 14)
    Index(['id', 'ts', 'Pclass', 'Age', 'SibSp', 'Parch', 'Sex_female', 'Sex_male',
           'Embarked_C', 'Embarked_Q', 'Embarked_S', 'prediction_probability',
           'prediction_value', 'label'],
          dtype='object')


- Model


```python
mdl = cp.load(urlopen("https://storage.googleapis.com/superwise/sdk/model.pickle"))
```

    /usr/local/lib/python3.7/site-packages/sklearn/base.py:318: UserWarning: Trying to unpickle estimator DecisionTreeClassifier from version 0.19.2 when using version 0.22. This might lead to breaking code or invalid results. Use at your own risk.
      UserWarning)
    /usr/local/lib/python3.7/site-packages/sklearn/base.py:318: UserWarning: Trying to unpickle estimator RandomForestClassifier from version 0.19.2 when using version 0.22. This might lead to breaking code or invalid results. Use at your own risk.
      UserWarning)


## Predict


```python
data['prediction_value'] = mdl.predict(data[FEATURES])
data['prediction_probability'] = mdl.predict_proba(data[FEATURES])[:,1]
```

## Start send to Superwise

* ensure env were declared


```python
os.environ["SW_HOST"]
os.environ["SW_TOKEN"]
```




    'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1Nzg4MzY3MDQsIm5iZiI6MTU3ODgzNjcwNCwianRpIjoiYmMzN2NjMGQtZGFiOS00YTdmLWIzZGItZTYzNjY0NmZiOGRlIiwiaWRlbnRpdHkiOjE0NzE1LCJmcmVzaCI6ZmFsc2UsInR5cGUiOiJhY2Nlc3MifQ.-dbZN7HbQZBEYLduMsEEVN1mX2korHG-Sdh9HTGzlto'



- Create a connection to superwise


```python
superwise_client = Superwise()
```

    2020-01-13 13:30:01,490 - superwise - INFO - Logg


### Send prediction emit request


```python
data['features'] = data[FEATURES].to_dict(orient='records')

prediction = data.loc[0]
prediction_to_send = {
    'id': str(prediction['id']),
    'ts': "2019-10-10 16:48:32",
    'features': prediction['features'],
    'prediction_probability': float(prediction['prediction_probability']),
    'prediction_value': str(prediction['prediction_value'])
}
prediction_to_send

superwise_client.trace.prediction_emit(data=prediction_to_send, task_id=3, version_id=1)
```

    2020-01-13 13:30:01,508 - superwise - INFO -  send https://dev.superwise.ai/v1/trace/task/3/prediction/emit request
    2020-01-13 13:30:02,612 - superwise - INFO - request https://dev.superwise.ai/v1/trace/task/3/prediction/emit  return with status code 200

    200



### Send prediction batch request


```python
dataset = data.iloc[0:100][['id','ts','features','prediction_probability','prediction_value']]
dataset['id'] = dataset['id'].astype(str)
dataset['prediction_value'] = dataset['prediction_value'].astype(str)
dataset['ts'] = "2019-10-10 16:48:32"
predictions = dataset.to_dict(orient='records')
```


```python
superwise_client.trace.prediction_batch(data=predictions, task_id=3, version_id=1,chunk_size=10)
```

    2020-01-13 13:30:02,661 - superwise - INFO -  send https://dev.superwise.ai/v1/trace/task/3/prediction/batch request


    100%|██████████| 10/10 [00:37<00:00,  3.75s/it]

    2020-01-13 13:30:40,141 - superwise - INFO - request https://dev.superwise.ai/v1/trace/task/3/prediction/batch  return with status code {200} and results {'{"failed_to_load":[]}\n'}

    {200}



### Send emit label request


```python
record = data.loc[0]
label_to_send = {
    'id': str(prediction['id']),
    'ts': "2019-10-10 16:48:32",
    'label' : str(prediction['label'])
}
label_to_send
superwise_client.trace.label_emit(data=label_to_send, task_id=3)
```

    2020-01-13 13:30:40,150 - superwise - INFO -  send https://dev.superwise.ai/v1/trace/task/3/label/emit request
    2020-01-13 13:30:41,020 - superwise - INFO - request https://dev.superwise.ai/v1/trace/task/3/label/emit  return with status code 200
    200


### Send label batch request


```python
dataset = data.iloc[0:100][['id','ts','label']]
dataset['id'] = dataset['id'].astype(str)
dataset['ts'] = "2019-10-10 16:48:32"
dataset['label'] = dataset['label'].astype(str)
labels = ds.to_dict(orient='records')

superwise_client.trace.label_batch(data=labels, task_id=3,chunk_size=10)
```

    2020-01-13 13:30:41,064 - superwise - INFO -  send https://dev.superwise.ai/v1/trace/task/3/label/batch request


    100%|██████████| 10/10 [01:10<00:00,  7.08s/it]

    2020-01-13 13:31:51,876 - superwise - INFO - request https://dev.superwise.ai/v1/trace/task/3/label/batch  return with status code {200} and results {'{"failed_to_load":[]}\n'}

    {200}
