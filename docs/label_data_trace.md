#### Label data

Label data is the feedback we get about your model, so we can check him against the growntruth.  

So, how we send label data?  

There are three ways to send label data to superwise:  

1. label emit request - send single record of data.  
2. label batch request- send multiple record at once.  
3. label file request - send a url to a csv with multiple record.  

Each requests should have another one parameter:  
1. task_id: int - task id  which reflects a project in superwise.    

Let's pass on each method!  

##### Label emit request
The data should be in this format:  
```
{
    id: string,
    ts: string,
    label: string
}
```
*Explain about the attributes:*     
* id - unique id of the record.   
* ts -  timestamp when the label was sent.   
* label - label for the record with the specific id.   
  
*EXAMPLE:*    
```python
from superwise.superwise import Superwise

sw = Superwise()

feedback = {
    "id": "1",
    "ts": "1998-04-18 16:48:37",
    "label": "0"
}
res = sw.trace.label_emit(data=feedback,task_id=1)
```
Excepted Response:
```
print(res)
'200'
```


### Label batch request
The data should be in this format:
```
[{feedback},{feedback},{...}]
```

Explain about the attributes:
* feedback - specific feedback
  
Another optional param is chunk_size, which reflects the amount of record per request,  
this param can improve performance, if you have a massive amount of data(default chunk_size is 10).  

*EXAMPLE:*  
```python
from superwise.superwise import Superwise

sw = Superwise()

my_doc = [
    {
        "id": "1",
        "ts": "1998-04-18 16:48:37",
        "label": "0"
    },
    {
        "id": "2",
        "ts": "1998-07-11 05:01:43",
        "label": "1"
    }
]
res = sw.trace.label_batch(data=my_doc,task_id=1, chunk_size=1)
```
Excepted Response:
```
print(res)
'{200}'
```
##### Label file request
File label request send multiple record from csv file store at our object storage.
For using that method you should upload your csv file to our bucket. 
*We support just in csv format*

*Explain about the attributes:*     
* file_url - path to the csv file. 

*EXAMPLE:*
```python
from superwise.superwise import Superwise

sw = Superwise()


res = sw.trace.label_file(file_url={file_path},task_id=1)
```
Excepted Response:
```
print(res)
'201'
```