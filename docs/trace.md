# Trace Service ![Screenshot](img/favicon_circle_sw.png)

Trace service let's you an easy way to send data to superwise.  
This data will help us monitor and understand your models behaviors.  

First, ensure you create a connection to superwise.  

There are two kind of requests for send data to superwise:    
1. Send predicted data to superwise.    
2. Send label data to superwise.  


* At all of the examples i'm using env token connection.

*Next we will explain how to send predicted data...*

