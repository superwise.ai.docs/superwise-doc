# API Docs ![Screenshot](img/favicon_circle_sw.png)
Here we will present another way to integrate superwise based on REST API.  
Each resource has is own api and is own request format.   
Each costumer has is own host url to work with superwise.  
The host url is based on the costumer.    
For example costumer "test" will use host: **`https://test.superwise.ai`**.  
* If you don't sure which host url you should use, please contact us.  
For start to consume our API, you should have **`TOKEN`** provided by us.

## Resources
- Trace


Let's start to pass on each resource documentation.

### Trace
Trace service let's you an easy way to send data to superwise.  
This data will help us monitor and understand your models behaviors.  

__Send Label__  
**`v1/trace/task/{task_id}/label/emit`**      
> Send the label for a prediction at a specific task.      
__Method:__ **`POST`**  
__Authorization:__ Bearer TOKEN  
__Content-Type:__ **`application/json`**  
__Required Body:__
```json
{  
    "id": "string",  
    "ts": "string",  
	"label": "string"  
}
```
> __Response__: 201   
  
  


__Send Label Batch__      
**`v1/trace/task/{task_id}/label/batch`**
> Send batch of labels for predictions at a specific task.        
__Method:__ **`POST`**  
__Authorization:__ Bearer TOKEN  
__Content-Type:__ **`application/json`**  
__Required Body:__
```json
{
  "records": "[{labels}]"
}
```
> __Response__: 201




__Send Prediction__  
**`v1/trace/task/{task_id}/prediction/emit`**        
> Send prediction at a specific task and version of a model.      
__Method:__ **`POST`**  
__Authorization:__ Bearer TOKEN    
__Content-Type:__ **`application/json`**  
__Required Body:__
```json
{
    "record":
    {
        "id": "string",
        "ts": "string",
        "features": 
        {
          "feature_name": "feature_value", 
        
        },
        "prediction_value": "string - NOT required", 
        "prediction_probability": "float"
    },
    "version_id": "int"
}

```
> __Response__: 201




__Send Predictions Batch__          
**`v1/trace/task/{task_id}/prediction/batch`**
> Send batch of predictions at a specific task and version of a model.  
__Method:__ **`POST`**  
__Authorization:__ Bearer TOKEN  
__Content-Type:__ **`application/json`**  
__Required Body:__
```json
{
  
  "records": "[{predictions}]",
  "version_id": "int"
  
}
```
> __Response__: 201


