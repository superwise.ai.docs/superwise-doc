#### Predicted data

Predicted data is the kind of data that you send to your model.  
This data will helps us to understand which segments do you have and what is the behavior of your model.    
 
So, how we send predicted data to superwise?  

There are three ways to send predicted data to superwise:  

1. prediction emit - send single record of data.
2. prediction_batch- send multiple record at once.
3. prediction_file - send a url to a csv with multiple record.

*Each request has a unique format.*

Each requests should have another 2 params:  
1. task_id: int - task id  which reflects a project in superwise.  
2. version_id: int - version id which reflects a version of the model.

Let's pass on each method!  

##### Prediction emit
As we mention emit request send single record of data.
The data should be in json format:
```
{
    id: string,
    ts: string,
    features:
        {
        feature_name: feature_value,
        ..
        },
    prediction_value: string - NOT required (depand on task type)
    prediction_probability: float
}
```
*Explain about the attributes:*    
* id - id of the sample.
* ts -  timestamp of the sample to be appear.
* data - key value of the features.
* prediction_value - the prediction class for classification tasks or scalar value for regression tasks.
* prediction_probability - the prediction probability for classification tasks.

*EXAMPLE:*  
```python
from superwise.superwise import Superwise

sw = Superwise()

record = {
    "id": "1",
    "ts": "1998-04-18 16:48:37",
    "features": { "country": "israel", "age" : 25 },
    "prediction_value": "1",
    "prediction_probability": 0.95
}
res = sw.trace.prediction_emit(data=record,task_id=1,version_id=1)
```
Excepted Response:
```
print(res)
'200'
```


##### Prediction batch
Batch request send multiple record of data.  
The data should be in this format:  
```
    [{record},{record},{...}]
```
Explain about the attributes:  
* record - specific prediction with it's content  

Another optional param is chunk_size, which reflects the amount of record per request,  
this param can improve performance, if you have a massive amount of data(default chunk_size is 10).   


*EXAMPLE:*
```python
from superwise.superwise import Superwise

sw = Superwise()

my_doc = [
    {
        "id": "1",
        "ts": "1998-04-18 16:48:37",
        "features": { "country": "israel", "age" : 25 },
        "prediction_value": "1",
        "prediction_probability": 0.95
    },
    {
        "id": "2",
        "ts": "1998-07-11 05:01:43",
        "features": { "country": "israel", "age" : 26 },
        "prediction_value": "0",
        "prediction_probability": 0.27
    }
]

res = sw.trace.prediction_batch(data=my_doc,task_id=3,version_id=1,chunk_size=8)
```
Excepted Response:
```
print(res)
'{200}'
```
##### Prediction file
File request send multiple record of data from url store at our object storage.  
The file supported just in csv format.  
For using that method you should upload your csv file to our bucket.   

*Explain about the attributes:*     
* file_url - path to the csv file. 

*EXAMPLE:*
```python
from superwise.superwise import Superwise

sw = Superwise()

res = sw.trace.prediction_file(file_url={file_path,task_id=1,version_id=1)  
```
Excepted Response:
```
print(res)
'201'
```

*Next we will explain how to send label data...*