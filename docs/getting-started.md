# Getting started ![Screenshot](img/favicon_circle_sw.png)

### Requirements

* For start to work with superwise, we first need a superwise.ai account.
If you don't have one, please [contact us](mailto:info@superwise.ai).  

### Installation
-  Let's install our sdk by the command:
```
    pip install superwise
```

- Also, there is an option to download our whl from [superwise-sdk]("")



### Env variables
The sdk will use few environment variables to indicate your private variables to identify you.  
- SW_HOST - the hostname you got from us.  
- SW_TOKEN - the token you got from us to start work with our sdk.    


After install our package and making sure you have the right ENV_VARS in place, open python shell.  


### Connection
First, we should create a connection to superwise, the connection use the SW_TOKEN env  
variable by default, Another option is to specify the token in the  connection request.   

* Using "SW_TOKEN" env   
```python
from superwise.superwise import Superwise

sw = Superwise()
```

* Using token implicit  
```python
from superwise.superwise import Superwise
token = "$2y$18$yOh7FJIL0DCGhqk1g0DTieUqZL3jSUT1vo3xOmfwZfip7G8rX2QYW"
sw = Superwise(token=token)
```

Excepted Output:  
```python
 "user Test_User success login"
```
*Next we will explain what is trace service and how to work with it...*

